//
// RoomsListViewModelTests.swift
        

import XCTest
import Combine
@testable import WeTransferRooms

class RoomsListViewModelTests: XCTestCase {
    var sut: RoomsListViewModel!
    var testCancellable: AnyCancellable!
    let interactor = RoomsListInteractorMock()
    override func setUpWithError() throws {
        try? super.setUpWithError()
        sut = RoomsListViewModel(interactor: interactor)
    }

    override func tearDownWithError() throws {
        sut = nil
        try? super.tearDownWithError()
    }

    func testBookingRoomSuccess() {
        // Given
        let room = Room(name: "Mostafa",
                        spots: 4,
                        thumbnail:   "https://images.unsplash.com/photo-1497366858526-0766cadbe8fa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80")
        let roomPresentable = RoomPresentableModel(room)
        let expectation = self.expectation(description: "Testing Booking a room should succeed")
        
        // When
        sut.onRoomBooking(room: roomPresentable)
        
        // Then
        testCancellable = sut
            .$isRoomBooked
            .dropFirst()
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    expectation.fulfill()
                }
            }) { (isRoomBooked) in
                XCTAssertTrue(isRoomBooked)
                expectation.fulfill()
            }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testBookingRoomFailure() {
        // Given
        let room = Room(name: "Mostafa",
                        spots: 4,
                        thumbnail:   "https://images.unsplash.com/photo-1497366858526-0766cadbe8fa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80")
        let roomPresentable = RoomPresentableModel(room)
        let expectation = self.expectation(description: "Testing Booking a room should succeed")
        interactor.setEnableBookingRoomFailure(true)
        // When
        sut.onRoomBooking(room: roomPresentable)
        
        // Then
        testCancellable = sut
            .$isRoomBooked
            .dropFirst()
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    expectation.fulfill()
                }
            }) { (isRoomBooked) in
                XCTAssertFalse(isRoomBooked)
                expectation.fulfill()
            }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}

class RoomsListInteractorMock: RoomsListInteractorType {
    private var enableBookingRoomFailure = false
    func fetchRooms() -> AnyPublisher<[RoomPresentableModel]?, Error> {
        return Future<[RoomPresentableModel]?, Error> { promise in
            promise(.success(nil))
        }.eraseToAnyPublisher()
    }

    func bookRoom(room: RoomPresentableModel) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { [weak self] promise in
            guard let self = self else { return }
            promise(.success(!self.enableBookingRoomFailure))
        }.eraseToAnyPublisher()
    }
    
    func setEnableBookingRoomFailure(_ value: Bool) {
        self.enableBookingRoomFailure = value
    }
}

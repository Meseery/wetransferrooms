
import XCTest
import Combine
@testable import WeTransferRooms

class RoomsListInteractorTests: XCTestCase {
    var testCancellable: AnyCancellable?
    var sut: RoomsListInteractorType?
    var reachabilityProvider: APIReachabilityProviderType?
    
    func test_FetchRoomsRemotely() {
        reachabilityProvider = APIReachabilityMock(isAPIReachable: true)
        sut = RoomsListInteractor(reachabilityProvider: reachabilityProvider!)
        let exp = self.expectation(description: "Fetching Rooms")
        testCancellable = sut?
            .fetchRooms()
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (rooms) in
                XCTAssertNotNil(rooms)
                XCTAssert(rooms?.count == 8)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_FetchRoomsLocally() {
        reachabilityProvider = APIReachabilityMock(isAPIReachable: false)
        sut = RoomsListInteractor(reachabilityProvider: reachabilityProvider!)
        let exp = self.expectation(description: "Fetching Rooms")
        testCancellable = sut?
            .fetchRooms()
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (rooms) in
                XCTAssertNotNil(rooms)
                XCTAssert(rooms?.count == 8)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_BookRoomRemotely() {
        reachabilityProvider = APIReachabilityMock(isAPIReachable: true)
        sut = RoomsListInteractor(reachabilityProvider: reachabilityProvider!)
        let exp = self.expectation(description: "Booking Room should succeed")
        testCancellable = sut?
            .bookRoom(room: RoomPresentableModel.stubRoom)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (isRoomBooked) in
                XCTAssertTrue(isRoomBooked)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_BookRoomWithoutAPIReachability() {
        reachabilityProvider = APIReachabilityMock(isAPIReachable: false)
        sut = RoomsListInteractor(reachabilityProvider: reachabilityProvider!)
        let exp = self.expectation(description: "Booking Room should fail without API reachability")
        testCancellable = sut?
            .bookRoom(room: RoomPresentableModel.stubRoom)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (isRoomBooked) in
                XCTAssertFalse(isRoomBooked)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }

    override func tearDown() {
        testCancellable?.cancel()
        sut = nil
        reachabilityProvider = nil
    }
}



extension RoomPresentableModel {
    static var stubRoom: RoomPresentableModel {
        .init(Room(name: "Mostafa",
                   spots: 4,
                   thumbnail:   "https://images.unsplash.com/photo-1497366858526-0766cadbe8fa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80"))
    }
}

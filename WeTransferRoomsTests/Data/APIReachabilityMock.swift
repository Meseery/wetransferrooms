
@testable import WeTransferRooms

struct APIReachabilityMock: APIReachabilityProviderType {
    var isAPIReachable: Bool
}

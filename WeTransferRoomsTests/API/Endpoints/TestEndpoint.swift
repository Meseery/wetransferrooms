
@testable import WeTransferRooms

struct TestEndpoint: APIEndpointType {
    var parameters: HTTPRequestParameters? { nil }
    
    var httpMethod: HTTPMethod { .GET }
    
    var environment: APIEnvironmentType { TestAPIEnvironment() }
    
    var path: String? { return "/todos/1"}
    
    var headers: HTTPHeaders? { return nil }
    
    var scheme: String? { return nil }
}

struct TestAPIEnvironment: APIEnvironmentType {
    var baseURL: String {"jsonplaceholder.typicode.com" }
    
    var scheme: String { "https" }
}

struct Todo: Decodable {
    let userId: Int, id: Int, title: String, completed: Bool
}

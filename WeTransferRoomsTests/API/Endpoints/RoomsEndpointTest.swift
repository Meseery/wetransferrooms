import XCTest
import Combine
@testable import WeTransferRooms

class RoomsEndpointTest: XCTestCase {
    var cancellable: AnyCancellable?
    
    func test_fetchRooms() {
        let roomsEndpoint = RoomsEndpoint.roomsList
        let exp = self.expectation(description: "Rooms Endpoint Test")
        cancellable = roomsEndpoint
            .execute(responseType: RoomsListResponse.self)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (response) in
                XCTAssertNotNil(response.rooms)
                XCTAssert(response.rooms?.count == 8)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    override func tearDown() {
        cancellable?.cancel()
    }
}

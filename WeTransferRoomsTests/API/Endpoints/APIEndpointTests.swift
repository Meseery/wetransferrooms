import XCTest
import Combine
@testable import WeTransferRooms

class APIEndpointTests: XCTestCase {
    var testCancellable: AnyCancellable?
    
    func test_APIEndpoint_Execute() {
        let testEndpoint = TestEndpoint()
        let exp = self.expectation(description: "Sample Endpoint execution")
        testCancellable = testEndpoint
            .execute(responseType: Todo.self)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
        }) { (todo) in
            XCTAssertNotNil(todo)
            exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_APIEndpoint_ExecuteWith_DecodingError() {
        let testEndpoint = TestEndpoint()
        let exp = self.expectation(description: "Sample Endpoint execution")
        testCancellable = testEndpoint
            .execute(responseType: [Todo].self)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    XCTAssert(error is APIError)
                    guard case APIError.parsingError = error else { XCTFail("Wrong Error Type"); return}
                    exp.fulfill()
                }
            }) { (todo) in
                XCTAssertNotNil(todo)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    override func tearDown() {
        testCancellable?.cancel()
    }
}

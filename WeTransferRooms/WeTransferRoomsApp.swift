import SwiftUI

@main
struct WeTransferRoomsApp: App {
    var body: some Scene {
        WindowGroup {
            RoomsListContentView()
        }
    }
}

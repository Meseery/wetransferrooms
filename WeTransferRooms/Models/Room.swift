

struct Room: Codable {
    let name: String?
    let spots: Int?
    let thumbnail: String?
}

enum RoomsEndpoint: APIEndpointType {
    case roomsList
    case bookRoom
        
    var httpMethod: HTTPMethod { .GET }
    
    var environment: APIEnvironmentType { APIEnvironment.qa }
    
    var path: String? {
        switch self {
            case .roomsList:
                return "/rooms.json"
            case .bookRoom:
                return "/bookRoom.json"
        }
    }

    var parameters: HTTPRequestParameters? { nil }
}

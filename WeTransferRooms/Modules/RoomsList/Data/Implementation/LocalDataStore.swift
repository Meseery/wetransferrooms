import Combine

struct LocalDataStore {
    // TODO: Setup local data store stack
    func fetchRoomsFromLocalStore() -> AnyPublisher<[RoomPresentableModel]?, Error> {
        let rooms = [
            Room(name: "Ljerka",
                           spots: 43,
                           thumbnail:  "https://images.unsplash.com/photo-1571624436279-b272aff752b5?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1504&q=80"),
            Room(name: "Mostafa",
                           spots: 4,
                           thumbnail:   "https://images.unsplash.com/photo-1497366858526-0766cadbe8fa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80"),
            Room(name: "Helmold", spots: 86, thumbnail: ""),
            Room(name: "Painter", spots: 16, thumbnail: ""),
            Room(name: "Septima", spots: 84, thumbnail: ""),
            Room(name: "Alexis", spots: 0, thumbnail: ""),
            Room(name: "Yana", spots: 34, thumbnail: ""),
            Room(name: "Mina", spots: 39, thumbnail: "")
        ].compactMap{RoomPresentableModel($0)}
        return Future<[RoomPresentableModel]?, Error> { promise in
            promise(.success(rooms))
        }.eraseToAnyPublisher()
    }
}

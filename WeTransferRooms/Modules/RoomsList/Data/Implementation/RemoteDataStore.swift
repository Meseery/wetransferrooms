import Combine

struct RemoteDataStore {
    func fetchRoomsFromRemoteStore() -> AnyPublisher<[RoomPresentableModel]?, Error> {
        RoomsEndpoint
            .roomsList
            .execute(responseType: RoomsListResponse.self)
            .map { $0.rooms?.compactMap { RoomPresentableModel($0) }}
            .eraseToAnyPublisher()
    }
}


import Combine

typealias RoomsListInteractorType = RoomsListFetchable & RoomBooking
struct RoomsListInteractor: RoomsListInteractorType {
    private let remoteStore: RemoteDataStore
    private let localStore: LocalDataStore
    private let reachabilityProvider: APIReachabilityProviderType
    
    init(remoteDataStore: RemoteDataStore = RemoteDataStore(),
         localDataStore: LocalDataStore = LocalDataStore(),
         reachabilityProvider: APIReachabilityProviderType = APIReachability()) {
        self.remoteStore = remoteDataStore
        self.localStore = localDataStore
        self.reachabilityProvider = reachabilityProvider
    }
    
    func fetchRooms() -> AnyPublisher<[RoomPresentableModel]?, Error> {
        if reachabilityProvider.isAPIReachable {
            return remoteStore.fetchRoomsFromRemoteStore()
        } else {
            return localStore.fetchRoomsFromLocalStore()
        }
    }
    
    func bookRoom(room: RoomPresentableModel) -> AnyPublisher<Bool, Error> {
        if reachabilityProvider.isAPIReachable {
            return RoomsEndpoint
                .bookRoom
                .execute(responseType: BookRoomResponse.self)
                .map {$0.success == true }
                .eraseToAnyPublisher()
        } else {
            return Future<Bool, Error> { promise in
                promise(.success(false))
            }.eraseToAnyPublisher()
        }
    }
}

import Combine

protocol RoomsListFetchable{
    func fetchRooms() -> AnyPublisher<[RoomPresentableModel]?, Error>
}

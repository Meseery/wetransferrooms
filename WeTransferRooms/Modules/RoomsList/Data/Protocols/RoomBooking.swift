import Combine

protocol RoomBooking {
    func bookRoom(room: RoomPresentableModel) -> AnyPublisher<Bool, Error>
}

import Foundation

final class RoomsListViewModel: ObservableObject, RoomsListViewModelContractor {
    @Published var roomsList = [RoomPresentableModel]()
    @Published var isRoomBooked: Bool = false
    
    private let interactor: RoomsListInteractorType
    
    init(interactor: RoomsListInteractorType = RoomsListInteractor()) {
        self.interactor = interactor
    }
    
    func onRoomsListViewAppear() {
            interactor
            .fetchRooms()
            .receive(on: RunLoop.main)
            .replaceNil(with: [])
            .replaceError(with: [])
            .assign(to: &$roomsList)
    }
    
    func onRoomBooking(room: RoomPresentableModel) {
            interactor
            .bookRoom(room: room)
            .receive(on: RunLoop.main)
            .replaceError(with: false)
            .assign(to: &$isRoomBooked)
    }
}

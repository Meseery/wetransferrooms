

protocol RoomsListViewModelContractor {
    var roomsList: [RoomPresentableModel] { get }
    var isRoomBooked: Bool { get }
}

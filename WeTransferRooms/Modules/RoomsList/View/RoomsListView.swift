import SwiftUI

struct RoomsListView: View {
    private var items: [RoomPresentableModel]
    private var onRoomBooking: (RoomPresentableModel) -> Void
    
    init(items: [RoomPresentableModel], onRoomBooking: @escaping (RoomPresentableModel) -> Void) {
        self.items = items
        self.onRoomBooking = onRoomBooking
    }
    
    var body: some View {
        Text("WeTransfer Rooms")
            .modifier(Title())
        List(items) { item in
            RoomItemView(room: item, onRoomBooking: onRoomBooking)
                .listRowSeparator(.hidden)
                .listRowBackground(Color.clear)
        }.listStyle(GroupedListStyle())
    }
}

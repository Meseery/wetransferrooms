import SwiftUI

struct RoomItemView: View {
    private var room: RoomPresentableModel
    private var onRoomBooking: (RoomPresentableModel) -> Void
    
    init(room: RoomPresentableModel,
         onRoomBooking: @escaping (RoomPresentableModel) -> Void) {
        self.room = room
        self.onRoomBooking = onRoomBooking
    }
    
    var body: some View {
        HStack(alignment: .center) {
            AsyncImage(url: room.roomImageUrl)
                .frame(width: 50, height: 50)
                .background(Circle().fill(Color.gray))
                .clipShape(Circle())
                .padding(.all, 10)
            
            VStack(alignment: .leading) {
                Text(room.roomName)
                    .font(.system(size: 18, weight: .bold, design: .default))
                    .foregroundColor(.white)
                Text(room.roomSpots)
                    .font(.system(size: 14, weight: .medium, design: .default))
                    .foregroundColor(.gray)
                HStack {
                    Text(room.roomStatus)
                        .font(.system(size: 14, weight: .light, design: .default))
                        .foregroundColor(.white)
                }
            }.padding(.trailing, 5)
            Spacer()
            Button {
                onRoomBooking(room)
            } label: {
                Text("Book")
                    .frame(width: 85, height: 25)
                    .foregroundColor(Color.white)
                    .background(Color.purple)
                    .modifier(CardModifier())
            }.padding(.all, 5)
            .if(!room.isRoomAvailable,
                transform: {$0.hidden()})
        }
        .padding(.all, 5)
        .frame(maxWidth: .infinity, alignment: .center)
        .background(Color(red: 32/255, green: 36/255, blue: 38/255))
        .modifier(CardModifier())
    }
}

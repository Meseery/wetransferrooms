import SwiftUI
import Combine

struct RoomsListContentView: View {
    @ObservedObject var viewModel: RoomsListViewModel = RoomsListViewModel()
    
    var body: some View {
        RoomsListView(items: viewModel.roomsList,
                      onRoomBooking: viewModel.onRoomBooking(room:))
            .alert("Room Booked",
                   isPresented: $viewModel.isRoomBooked,
                   actions: {})
            .onAppear(perform: viewModel.onRoomsListViewAppear)
    }
}

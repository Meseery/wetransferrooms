import Foundation

struct RoomPresentableModel {
    private let room: Room
        
    init(_ room: Room) {
        self.room = room
    }
    
    var roomName: String { room.name ?? "WeTransfer Room" }
    
    var roomImageUrl: URL? { URL(string: room.thumbnail ?? "") }
    
    var roomSpots: String {"\(room.spots ?? 0) spots remaining"}
}
                                 
extension RoomPresentableModel: Identifiable {
    var id: String { room.name ?? ""}
}

extension RoomPresentableModel {
    var roomStatus: String { isRoomAvailable ? "available" : "fully-booked"}
    
    var isRoomAvailable: Bool { (room.spots ?? 0) > 0 }
}



protocol APIEndpointType {
    var path: String? { get }
    var httpMethod: HTTPMethod { get }
    var parameters: HTTPRequestParameters? { get }
    var extraHeaders: HTTPHeaders? { get }
    var environment: APIEnvironmentType { get }
}



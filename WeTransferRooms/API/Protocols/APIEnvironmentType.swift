import Foundation

protocol APIEnvironmentType {
    var baseURL: String { get }
    var headers: HTTPHeaders? { get }
    var cachePolicy: URLRequest.CachePolicy { get }
    var timeout: Double { get }
    var scheme: String { get }
}

import Foundation

protocol APIErrorType: LocalizedError, CustomStringConvertible {}

import Foundation

protocol HTTPParametersEncoderType {
    static func encode(urlRequest: inout URLRequest,
                       with parameters: HTTPParameters) throws
}



protocol APIReachabilityProviderType {
    var isAPIReachable: Bool { get }
}

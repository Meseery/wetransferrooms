import Foundation
import Combine

extension APIEndpointType {
    func execute(onCompletion: @escaping CompletionClosure<Data,APIError>) {        
        do {
            let request = try prepareRequest()
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                guard error == nil, let response = response as? HTTPURLResponse  else {
                    onCompletion(.failure(APIError.networkException))
                    return
                }
                
                guard (200..<300).contains(response.statusCode) else {
                    onCompletion(.failure(APIError.networkError(errorCode: response.statusCode)))
                    return
                }
                
                guard let responseData = data else {
                    onCompletion(.failure(APIError.invalidData))
                    return
                }
                
                onCompletion(.success(responseData))
            }.resume()
        } catch let error as APIError {
            onCompletion(.failure(error))
        } catch {
            onCompletion(.failure(APIError.networkException))
        }
    }
}

extension APIEndpointType {
    func prepareRequest() throws -> URLRequest {
        var urlComponents = URLComponents()
        urlComponents.host = environment.baseURL
        urlComponents.scheme = environment.scheme
        
        if let path = path {
            urlComponents.path = path
        }
        
        guard let url = urlComponents.url else {
            throw APIError.invalidEndpoint
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        request.cachePolicy = environment.cachePolicy
        request.timeoutInterval = environment.timeout
        
        if let headers = extraHeaders {
            headers.forEach { request.addValue($0.value, forHTTPHeaderField: $0.key) }
        }
        
        if let environmentHeaders = environment.headers {
            environmentHeaders.forEach { request.addValue($0.value, forHTTPHeaderField: $0.key) }
        }
        
        if let parameters = parameters {
            do {
                try configureRequestParameters(request: &request,
                                               parameters: parameters)
            } catch let error {
                throw APIError.parameterEncoding(error: error as! APIParametersEncodingError)
            }
        }
        
        return request
    }
    
    func configureRequestParameters(request: inout URLRequest,
                                    parameters: HTTPRequestParameters) throws {
        do {
            switch parameters {
            case let .urlParameters(urlParameters):
                try APIURLParameterEncoder.encode(urlRequest: &request,
                                                  with: urlParameters)
            case let .bodyParameters(bodyParameters):
                try APIBodyParameterEncoder.encode(urlRequest: &request,
                                                   with: bodyParameters)
            case .urlAndBodyParameters(let urlParameters,
                                       let bodyParameters):
                try APIURLParameterEncoder.encode(urlRequest: &request,
                                                  with: urlParameters)
                try APIBodyParameterEncoder.encode(urlRequest: &request,
                                                   with: bodyParameters)
            }
        } catch {
            throw error
        }
    }
}

extension APIEndpointType {
    var extraHeaders: HTTPHeaders? { return nil }
}

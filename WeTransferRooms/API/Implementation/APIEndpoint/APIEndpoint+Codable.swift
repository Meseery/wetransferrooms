import Foundation

extension APIEndpointType {
    func execute<U: Decodable>(responseType: U.Type,
                               onCompletion: @escaping CompletionClosure<U, APIError>)  {
        execute { (result) in
            switch result {
            case let .success(responseData):
                do {
                    let decoded = try JSONDecoder().decode(U.self, from: responseData)
                    onCompletion(.success(decoded))
                } catch {
                    onCompletion(.failure(APIError.parsingError))
                }
            case let .failure( error ):
                onCompletion(.failure(error))
            }
        }
    }
}

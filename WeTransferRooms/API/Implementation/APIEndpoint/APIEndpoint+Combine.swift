import Foundation
import Combine

extension APIEndpointType {
    func execute() -> AnyPublisher<Data, Error>  {        
        guard let request = try? prepareRequest() else {
            return Fail(error: APIError.invalidEndpoint).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: request)
            .map {$0.data}
            .mapError { APIError.networkError(errorCode: ($0 ).errorCode) }
            .eraseToAnyPublisher()
    }
    
    func execute<U: Decodable>(responseType: U.Type) -> AnyPublisher<U, Error>  {
        guard let request = try? prepareRequest() else {
            return Fail(error: APIError.invalidEndpoint).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: request)
            .map {$0.data}
            .decode(type: U.self, decoder: JSONDecoder())
            .mapError {
                switch $0 {
                case is DecodingError: return APIError.parsingError
                case is URLError: return APIError.networkError(errorCode: ($0 as! URLError).errorCode)
                default: return APIError.networkException
                }
        }
        .eraseToAnyPublisher()
    }
}

import Foundation

enum APIError: APIErrorType {
    case invalidEndpoint
    case parameterEncoding(error: APIParametersEncodingError)
    case networkError(errorCode: Int)
    case networkException
    case parsingError
    case invalidData
    
    var description: String {
        switch self {
        case .invalidEndpoint:
            return "Invalid Endpoint"
        case .parsingError:
            return "Invalid Type"
        case .networkException:
            return "Something went wrong!"
        case .invalidData:
            return "Unable to fetch data!"
        case .parameterEncoding(let error):
            return error.description
        case let .networkError(errorCode):
            switch errorCode {
            case 404:
                return "Sorry, couldn't find data matching your query"
            default:
                return "Sorry! unable to serve you this time"
            }
        }
    }
}



enum APIParametersEncodingError: APIErrorType {
    case missingURL
    case missingParameters
    case failedEncoding
    
    var description: String {
        switch self {
            case .missingURL:
                return "Expected URL, Got nil"
            case .missingParameters:
                return "Expected Parameters, Got nil"
            case .failedEncoding:
                return "Expected Successfull encoding, Got failure"
        }
    }
}

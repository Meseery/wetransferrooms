import Foundation

struct APIURLParameterEncoder: HTTPParametersEncoderType {
     static func encode(urlRequest: inout URLRequest,
                       with parameters: HTTPParameters) throws {
        guard let url = urlRequest.url else { throw APIParametersEncodingError.missingURL }
        
        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false), !parameters.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in parameters {
                let queryItem = URLQueryItem(name: key, value: "\(value)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))
                urlComponents.queryItems?.append(queryItem)
            }
            urlRequest.url = urlComponents.url
        }
        
        urlRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
    }
}

struct APIBodyParameterEncoder: HTTPParametersEncoderType {
     static func encode(urlRequest: inout URLRequest,
                       with parameters: HTTPParameters) throws {
        guard urlRequest.url != nil else { throw APIParametersEncodingError.missingURL }
        
        guard let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            throw APIParametersEncodingError.missingParameters
        }
        urlRequest.httpBody = data
        urlRequest.setValue("application/json",
                            forHTTPHeaderField: "Content-Type")
    }
}

import Foundation

extension APIEnvironmentType {
    var headers: HTTPHeaders? { nil }

    var cachePolicy: URLRequest.CachePolicy { .useProtocolCachePolicy }
        
    var timeout: Double { 60.0 }
}

enum APIEnvironment: APIEnvironmentType {
    case qa
    
    var baseURL: String { "wetransfer.github.io" }
    
    var scheme: String { "https" }
}

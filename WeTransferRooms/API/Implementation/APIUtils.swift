

typealias HTTPParameters = [String: Any]
typealias HTTPHeaders = [String:String]
typealias CompletionClosure<T,E:Error> = (Result<T,E>) -> Void

enum HTTPRequestParameters {
    case urlParameters(parameters: HTTPParameters)
    case bodyParameters(parameters: HTTPParameters)
    case urlAndBodyParameters(urlParameters: HTTPParameters, bodyParameters: HTTPParameters)
}

enum HTTPMethod: String {
    case OPTIONS = "OPTIONS"
    case GET = "GET"
    case HEAD = "HEAD"
    case POST = "POST"
    case PUT = "PUT"
    case PATCH = "PATCH"
    case DELETE = "DELETE"
    case TRACE = "TRACE"
    case CONNECT = "CONNECT"
}

# WeTransferRooms App

### Description

A small company has an internal system to handle their meeting rooms. They already have a working version for web (see design), but are now interested in making a native iOS application.

Write a simple client for iOS that handles the following user scenarios:
- A user wants to be able to see all of the rooms available
- A user wants to book a room if it has available spots

### Specs

Use your best judgment to convert the web design to a mobile friendly one

- [Design](https://www.figma.com/file/SVCALDDXuK010oEEzo6Bn9/Book-A-Room?node-id=0%3A1 )
- Fetch the meeting rooms thanks to the following RESTful API endpoint:
    [url](https://wetransfer.github.io/rooms.json)
- When a user books a room, call the following RESTful API endpoint:
    [url](https://wetransfer.github.io/bookRoom.json)
- The list of rooms should be available offline

### Delivery

- Deliver your assignment by sending us the link to your repository (on Github, Gitlab or Bitbucket, etc.).

- Include a README file detailing your approach.

- Spend a maximum of 3 hours on this assignment. If you run out of time, just describe what you would've done in the README.

- Write your code as if it were production code

## Implementation

Single screen iOS App written using Swift 5 with MVVVM architecture. 

### Prerequisites

Xcode 13
Swift 5
iOS 15

### Required User Stories:

#### US-1
As an employee, I want to be able to view a list of rooms at office.

`ACCEPTANCE CRITERIA`

- The list should include the `name` of the room, the `spots` available and the room `Image`.

#### US-2
As an employee, I want to be able to book a room if available.

`ACCEPTANCE CRITERIA`

- A button in rooms list should be enough for booking a room.

#### US-3
As an employee, I want to be able to list rooms for offline viewing.

`ACCEPTANCE CRITERIA`

- Rooms list could be listed offline.

### Good Vibes!

* App is built in a protocol oriented paradigm where each entity is represented by first class citizen protocol.
* App is functionaly tested with test coverage by ```64%``` .

### Improvements

* Add local storage service for storing elements retrieved from BE.
* Separate entity for handling booking rooms for better separation of concerns.
* Cover the App with more funcitonal and UI testing.
